const gUserInfo = {
  firstname: 'Hoang',
  lastname: 'Pham',
  avatar: 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
  age: 30,
  language: ['Vietnamese', 'Japanese', 'English']
}


function App() {
  return (
    <div className="App">
      <h5>Ho ten User: {gUserInfo.lastname} {gUserInfo.firstname}</h5>
      <img src={gUserInfo.avatar} width='300'></img>
      <p>Tuoi user: {gUserInfo.age}</p>
      <p>{gUserInfo.age <= 25 ? 'Anh ay con tre' : 'Anh ay da gia'}</p>
      <ul>
        {
          gUserInfo.language.map((value, index) => {
            return <li key={index}>{value}</li> 
          })
        }
      </ul>
    </div>
  );
}

export default App;
